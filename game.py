from random import randint
name = input("Hi! What is your name? ")
for num in range(1,6):
    number_between_1_and_12 = randint(1, 12)
    number_between_1924_and_2004 = randint(1924, 2004)
    print("Guess ", num, ":", name, "were you born in", number_between_1_and_12, "/", number_between_1924_and_2004, "?" )
    answer = input("yes or no: ")
    if answer == "yes":
        print("I knew it!")
        exit()
    else:
        if num == 5:
            print("I have other things to do goodbye.")
            exit()
        print("Drat! Lemme try again!")
